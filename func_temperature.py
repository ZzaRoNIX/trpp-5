def CinF(a):
    return a * 9 / 5 + 32


def CinK(a):
    return a + 273.15


def FinC(a):
    return (a - 32) * 5 / 9


def FinK(a):
    return FinC(a) + 273.15


def KinC(a):
    return a - 273.15


def KinF(a):
    return CinF(KinC(a))


def temperature(temperature, origin_scale, dest_scale):
    origin_scale = origin_scale.upper()
    dest_scale = dest_scale.upper()
    file = open("test.txt", "a")
    if (origin_scale != "C" and origin_scale != "F" and origin_scale != "K") or (dest_scale != "C" and dest_scale != "F" and dest_scale != "K"):
        file.write("Ошибка шкалы\n")
        return "Ошибка шкалы"
    if origin_scale == "C":
        if dest_scale == "F":
            temp = CinF(temperature)
        if dest_scale == "K":
            temp = CinK(temperature)
    else:
        if origin_scale == "F":
            if dest_scale == "C":
                temp = FinC(temperature)
            if dest_scale == "K":
                temp = FinK(temperature)
        if origin_scale == "K":
            if dest_scale == "C":
                temp = KinC(temperature)
            if dest_scale == "F":
                temp = KinF(temperature)
        if origin_scale == dest_scale:
            temp = temperature
    file.write(str(temperature) + " " + origin_scale + " = " + str(temp) + " " + dest_scale + "\n")
    return temp




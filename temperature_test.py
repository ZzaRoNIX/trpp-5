from func_temperature import temperature


def test_celTokel():
    assert temperature(10, 'cel', 'kel') == 283

def test_kelTocel():
    assert temperature(10, 'kel', 'cel') == -263

def test_celTofah():
    assert temperature(10, 'cel', 'fah') == 50

def test_fahTocel():
    assert temperature(10, 'fah', 'cel') == -12

def test_kelTofah():
    assert temperature(10, 'kel', 'fah') == -441

def test_fahTokel():
    assert temperature(10, 'fah', 'kel') == 260